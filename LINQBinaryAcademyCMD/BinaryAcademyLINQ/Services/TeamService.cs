﻿using Lecture1_API.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BinaryAcademyLINQ.BL.Services
{
    public class TeamService
    {
        private readonly IUserController _userController;
        private readonly ITeamController _teamController;
        public TeamService(ITeamController teamController, IUserController userController)
        {
            _userController = userController;
            _teamController = teamController;
        }

        //4.Отримати список(id, ім'я команди і список користувачів) з колекції команд, учасники яких старші 10 років, відсортованих за датою реєстрації користувача за спаданням, а також згрупованих по командах.
        //P.S -в цьому запиті допускається перевірка лише року народження користувача, без прив'язки до місяця/дня/часу народження.
        public IEnumerable<Object> GetSeniorEmployees()
        {
            var curentDate = DateTime.Now;
            const int minAge = 10;
            try
            {
                var users = _userController.GetUsers();
                var teams = _teamController.GetTeams();

                return teams
                    .GroupJoin(users,
                    t => t.Id,
                    u => u.TeamId,
                    (t, u) => 
                    new { TeamID = t.Id, TeamName = t.Name, Users = u.Where(u => u.BirthDay.Year < curentDate.Year - minAge).OrderByDescending(u => u.RegisteredAt).ToList() })
                    .ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}
