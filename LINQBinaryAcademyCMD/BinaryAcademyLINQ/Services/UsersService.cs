﻿using Lecture1_API.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BinaryAcademyLINQ.BL.Services
{
    public class UsersService
    {
        private readonly IProjectController _projectController;
        private readonly ITaskController _taskController;
        private readonly IUserController _userController;
        public UsersService(IUserController userController, ITaskController taskController, IProjectController projectController)
        {
            _projectController = projectController;
            _userController = userController;
            _taskController = taskController;
        }

        //5.Отримати список користувачів за алфавітом first_name(по зростанню) з відсортованими tasks по довжині name(за спаданням).
        public IEnumerable<object> GetUsersTasks()
        {
            try
            {
                var users = _userController.GetUsers();
                var tasks = _taskController.GetTasks();

                return users.GroupJoin(tasks,
                    u => u.Id,
                    t => t.PerformerId,
                    (u, t) => new { User = u, Tasks = t.OrderByDescending(t => t.Name.Length) }).OrderBy(u => u.User.FirstName).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

//6.Отримати наступну структуру(передати Id користувача в параметри) :
//User
//Останній проект користувача(за датою створення)
//Загальна кількість тасків під останнім проектом
//Загальна кількість незавершених або скасованих тасків для користувача
//Найтриваліший таск користувача за датою(найраніше створений - найпізніше закінчений)
//P.S. - в даному випадку, статус таска не має значення, фільтруємо тільки за датою.
        public IEnumerable<Object> GetUserEfficiency(int id)
        {
            try
            {
                var projects = _projectController.GetProjects();
                var users = _userController.GetUsers();
                var tasks = _taskController.GetTasks();

                var result =
                    users
                    .Join(projects,
                    u => u.Id,
                    p => p.AuthorId,
                    (u, p) => new { User = u, Project = p })
                    .GroupJoin(tasks,
                    u => u.User.Id,
                    t => t.PerformerId,
                    (u, t) => new { u, t })
                    .Where(u => u.u.User.Id == id)
                    .Select(s => new
                    {
                        User = s.u.User,
                        LastProject = s.u.Project,
                        LastProjectTasks = s.t.Where(t => t.ProjectId == s.u.Project.Id).Count(),
                        CancellationTasks = s.t.Where(t => !t.FinishedAt.HasValue),
                        LongestTask = s.t.OrderBy(x => x.CreatedAt).ThenByDescending(x => x.FinishedAt).FirstOrDefault()
                    })
                    .FirstOrDefault();

                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw new NotImplementedException();
            }
        }

    }
}
