﻿using Lecture1_API.ClassBase;
using Lecture1_API.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BinaryAcademyLINQ.BL.Services
{
    public class ProjectService
    {

        private readonly IProjectController _projectController;
        private readonly ITaskController _taskController;
        public ProjectService(IProjectController projectController, ITaskController taskController)
        {
            _projectController = projectController;
            _taskController = taskController;
        }

        //1.Отримати кількість тасків у проекті конкретного користувача (по id) (словник, де key буде проект, а value кількість тасків)
        public IDictionary<Project, int> GetProjectTask(int id)
        {
            try
            {
                var projects = _projectController.GetProjects();
                var tasks = _taskController.GetTasks();

                return projects
                   .GroupJoin(tasks,
                   p => p.Id,
                   t => t.ProjectId,
                   (p, t) => new { Project = p, Tasks = t.Where(t => t.PerformerId == id && t.ProjectId == p.Id).Count() })
                   .Where(t => t.Tasks > 0)
                   .ToDictionary(p => p.Project, t => t.Tasks);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}
