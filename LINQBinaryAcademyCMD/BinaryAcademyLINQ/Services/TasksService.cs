﻿using Lecture1_API.ClassBase;
using Lecture1_API.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BinaryAcademyLINQ.BL.Services
{
    public class TasksService
    {

        private readonly ITaskController _taskController;
        public TasksService(ITaskController taskController)
        {
            _taskController = taskController;
        }

        //2.Отримати список тасків, призначених для конкретного користувача (по id), де name таска <45 символів (колекція з тасків).
        public IEnumerable<Task> GetUserTasks(int id)
        {
            const int maxLength = 45;
            try
            {
                var tasks = _taskController.GetTasks();

                return tasks.Where(t => t.Name.Length > maxLength).Where(t => t.PerformerId == id).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        //3.Отримати список(id, name) з колекції тасків, які виконані(finished) в поточному(2021) році для конкретного користувача(по id).
        public IEnumerable<Object> GetFinishedTasks(int id)
        {
            const int currentYear = 2021;
            try
            {
                var tasks = _taskController.GetTasks();

                var result =  tasks.Where(t =>t.FinishedAt.HasValue && t.FinishedAt?.Year == currentYear).Where(t => t.PerformerId == id).Select(t => new { Id = t.Id, Name = t.Name });
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}
