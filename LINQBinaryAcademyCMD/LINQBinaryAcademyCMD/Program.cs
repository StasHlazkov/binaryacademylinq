﻿using BinaryAcademyLINQ.BL.Services;
using Lecture1_API;
using Lecture1_API.Controllers;
using System;

namespace LINQBinaryAcademyCMD
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Api Context
            ApiContext context = new ApiContext("https://bsa21.azurewebsites.net");
            #endregion

            #region Controllers
            var userController = new UserController(context);
            var projectController = new ProjectController(context);
            var taskController = new TaskController(context);
            var teamController = new TeamController(context);
            #endregion

            #region Services
            ProjectService project = new ProjectService(projectController, taskController);
            TasksService tasksService = new TasksService(taskController);
            UsersService usersService = new UsersService(userController, taskController, projectController);
            TeamService teamService = new TeamService(teamController, userController);
            #endregion

            #region Tasks Methods
            //1)
            var result1 = project.GetProjectTask(20);
            //2)
            var result2 = tasksService.GetUserTasks(20);
            //3)
            var result3 = tasksService.GetFinishedTasks(20);
            //4)
            var result4 = teamService.GetSeniorEmployees();
            //5)
            var result5 = usersService.GetUsersTasks();
            //6) 
            var result6 = usersService.GetUserEfficiency(65);
            #endregion

            Console.ReadLine();
        }
    }
}
