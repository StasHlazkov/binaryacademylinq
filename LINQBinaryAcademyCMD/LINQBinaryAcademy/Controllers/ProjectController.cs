﻿using Lecture1_API.ClassBase;
using Lecture1_API.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture1_API.Controllers
{
    public class ProjectController : BaseController, IProjectController
    {
        public ProjectController(ApiContext apiContext) : base(apiContext)
        {

        }

        public Project GetProjectById(int id)
        {
            var response = GetResponse("/Projects", id);
            return JsonFormatter.ConvertFromJson<Project>(response);
        }

        public IEnumerable<Project> GetProjects()
        {
            var response = GetResponse("/Projects");
            return JsonFormatter.ConvertFromJson<IEnumerable<Project>>(response);
        }
    }
}
