﻿using Lecture1_API.ClassBase;
using Lecture1_API.Interfaces;
using System.Collections.Generic;

namespace Lecture1_API.Controllers
{
    public class UserController : BaseController, IUserController
    {
        public UserController(ApiContext apiContext) : base(apiContext)
        {

        }

        public User GetUserById(int id)
        {
            var response = GetResponse("/Users",id);
            return JsonFormatter.ConvertFromJson<User>(response);
        }

        public IEnumerable<User> GetUsers()
        {
            var response = GetResponse("/Users");
            return JsonFormatter.ConvertFromJson<IEnumerable<User>>(response);
        }

    }
}
