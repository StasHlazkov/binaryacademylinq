﻿using Lecture1_API.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lecture1_API.ClassBase;
using Task = Lecture1_API.ClassBase.Task;

namespace Lecture1_API.Controllers
{
    public class TaskController : BaseController, ITaskController
    {
        public TaskController(ApiContext apiContext) : base(apiContext)
        {

        }

        public Task GeTaskById(int id)
        {
            var response = GetResponse("/Tasks", id);
            return JsonFormatter.ConvertFromJson<Task>(response);
        }

        public IEnumerable<Task> GetTasks()
        {
            var response = GetResponse("/Tasks");
            return JsonFormatter.ConvertFromJson<IEnumerable<Task>>(response);
        }
    }
}
