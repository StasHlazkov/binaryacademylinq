﻿using Lecture1_API.ClassBase;
using Lecture1_API.Interfaces;
using System.Collections.Generic;

namespace Lecture1_API.Controllers
{
    public class TeamController : BaseController, ITeamController
    {
        public TeamController(ApiContext apiContext) : base(apiContext)
        {

        }

        public Team GetTeamById(int id)
        {
            var response = GetResponse("/Teams", id);
            return JsonFormatter.ConvertFromJson<Team>(response);
        }

        public IEnumerable<Team> GetTeams()
        {
            var response = GetResponse("/Teams");
            return JsonFormatter.ConvertFromJson<IEnumerable<Team>>(response);
        }
    }
}
