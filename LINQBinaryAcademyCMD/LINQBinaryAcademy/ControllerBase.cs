﻿
namespace Lecture1_API
{
    public abstract class BaseController : IBaseController
    {
        private ApiContext ApiContext { get; set; }

        public BaseController(ApiContext apiContext)
        {
            ApiContext = apiContext;
        }

        public string GetResponse(string extension, string httpMethod = "GET")
        {
            var response = Factory.GetResponse(extension, ApiContext, httpMethod);
            return response;
        }
        public string GetResponse(string extension, int Id, string httpMethod = "GET")
        {
            extension = extension + "/" + Id;
            var response = Factory.GetResponse(extension, ApiContext, httpMethod);
            return response;
        }
    }

    public interface IBaseController
    {
        string GetResponse(string extension, string httpMethod = "GET");
        string GetResponse(string extension, int Id, string httpMethod = "GET");
    }
}
