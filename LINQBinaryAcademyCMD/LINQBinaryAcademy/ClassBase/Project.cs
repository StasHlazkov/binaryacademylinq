﻿using Lecture1_API.BaseClass;
using System;

namespace Lecture1_API.ClassBase
{
    public class Project : ApiObject
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
    }
}
