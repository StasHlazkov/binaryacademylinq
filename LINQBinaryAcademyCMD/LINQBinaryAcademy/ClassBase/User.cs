﻿using Lecture1_API.BaseClass;
using System;

namespace Lecture1_API.ClassBase
{
    public class User : ApiObject
    {
        public int Id { get; set; }
        public int TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
    }
}
