﻿using Lecture1_API.BaseClass;
using System;

namespace Lecture1_API.ClassBase
{
    public class Team : ApiObject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
