﻿using Lecture1_API.ClassBase;
using System.Collections.Generic;

namespace Lecture1_API.Interfaces
{
    public interface ITeamController
    {
        IEnumerable<Team> GetTeams();
        Team GetTeamById(int id);
    }
}
