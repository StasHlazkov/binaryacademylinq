﻿using Lecture1_API.ClassBase;
using System.Collections.Generic;

namespace Lecture1_API.Interfaces
{
    public interface IProjectController
    {
        IEnumerable<Project> GetProjects();
        Project GetProjectById(int id);
    }
}
