﻿using System.Collections.Generic;
using Task = Lecture1_API.ClassBase.Task;

namespace Lecture1_API.Interfaces
{
    public interface ITaskController
    {
        IEnumerable<Task> GetTasks();
        Task GeTaskById(int id);
    }
}
