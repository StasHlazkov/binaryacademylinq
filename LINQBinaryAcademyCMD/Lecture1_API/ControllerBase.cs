﻿
namespace Lecture1_API
{
    public abstract class BaseController : IBaseController
    {
        private ApiContext ApiContext { get; set; }

        public BaseController(ApiContext apiContext)
        {
            ApiContext = apiContext;
        }

        public string GetResponse(string extension, string httpMethod = "POST")
        {
            var response = Factory.GetResponse(extension, ApiContext, httpMethod);
            return response;
        }
    }

    public interface IBaseController
    {
        string GetResponse(string extension, string httpMethod = "POST");
    }
}
