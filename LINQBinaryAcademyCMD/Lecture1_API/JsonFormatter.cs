﻿using Lecture1_API.BaseClass;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture1_API
{
    public static class JsonFormatter
    {
        public static JsonSerializerSettings SerializerSettingsDateFormat = new JsonSerializerSettings() { DateFormatString = "yyyy-MM-ddTHH:mm:ss.ffZ" };
        public static JsonSerializerSettings SerializerSettingsIgnoreNullValue = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };

        public static T ConvertFromJson<T>(string value)
        {
            var obj = JsonConvert.DeserializeObject<T>(value, SerializerSettingsIgnoreNullValue);
            if (obj is ApiObject)
            {
                (obj as ApiObject).Json = value;
            }
            if (obj is IEnumerable)
            {
                var collection = obj as IEnumerable;
                foreach (var item in collection)
                {
                    if (item is ApiObject)
                    {
                        (item as ApiObject).Json = value;
                    }
                }
            }
            return obj;
        }

        public static string ConvertToJson<T>(T t)
        {
            return JsonConvert.SerializeObject(t, SerializerSettingsDateFormat);
        }
    }
}
