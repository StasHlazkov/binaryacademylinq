﻿using System;
using System.IO;
using System.Net;

namespace Lecture1_API
{
    public class Factory
    {
        public static string GetResponse(string extension, ApiContext context, string httpMethod)
        {
            string url = context.ApiServer + extension;
            var req = HttpWebRequest.Create(url);
            req.Method = httpMethod;
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = 0;

            var response = (HttpWebResponse)req.GetResponse();
            string responseBody = "";

            using (StreamReader sr = new StreamReader(response.GetResponseStream()))
            {
                responseBody = sr.ReadToEnd();
            }

            if ((int)response.StatusCode < 200 || (int)response.StatusCode > 299)
            {
                var error = JsonFormatter.ConvertFromJson<ApiError>(responseBody);
                throw new Exception(error.Message);
            }

            return responseBody;
        }

        public class ApiError
        {
            public string Message { get; set; }
        }
    }
}
