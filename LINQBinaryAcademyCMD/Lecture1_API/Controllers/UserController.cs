﻿using Lecture1_API.ClassBase;
using Lecture1_API.Interfaces;
using System.Collections.Generic;

namespace Lecture1_API.Controllers
{
    public class UserController : BaseController, IUserController
    {
        public UserController(ApiContext apiContext) : base(apiContext)
        {

        }

        public IEnumerable<User> GetUsers()
        {
            var response = GetResponse("/api/Users");
            return JsonFormatter.ConvertFromJson<IEnumerable<User>>(response);
        }

    }
}
