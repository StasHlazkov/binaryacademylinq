﻿using System;

namespace Lecture1_API
{
    public class ApiContext
    {
        public string ApiServer { get; private set; }

        public ApiContext(string apiServer)
        {
            if (string.IsNullOrWhiteSpace(apiServer))
                throw new ArgumentNullException(nameof(apiServer), "ApiServer is missing");

            ApiServer = apiServer + (apiServer.EndsWith("/api/") ? "" : "/api/");
        }
    }
}
