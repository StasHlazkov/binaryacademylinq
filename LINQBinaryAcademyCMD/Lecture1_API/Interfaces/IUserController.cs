﻿using Lecture1_API.ClassBase;
using System.Collections.Generic;

namespace Lecture1_API.Interfaces
{
    public interface IUserController
    {
        IEnumerable<User> GetUsers();
    }
}
